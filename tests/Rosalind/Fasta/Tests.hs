module Rosalind.Fasta.Tests where

import           Test.Hspec
import           Test.Tasty          (TestTree)
import           Test.Tasty.Hspec    (testSpec)

import           Data.HashMap.Strict as M

import           Rosalind.Fasta

test :: IO [TestTree]
test = sequence
  [ testSpec "FASTA" testFasta
  ]

testFasta :: Spec
testFasta =
  describe "Parse FASTA formatted sequences" $ do
    it "Parses an empty string" $
      parseFasta "" `shouldBe` M.empty

    it "Parses an empty entry" $
      parseFasta ">" `shouldBe` M.empty

    it "Parses a single entry" $
      parseFasta ">test\nABCDEF" `shouldBe` M.fromList [("test", "ABCDEF")]

    it "Parses a single entry over multiple lines" $
      parseFasta ">test\nABC\nDEF" `shouldBe` M.fromList [("test", "ABCDEF")]

    it "Parses an entry with a preceding space in the header" $
      parseFasta "\n> test\n\nABC" `shouldBe` M.singleton "test" "ABC"

    it "Parses an entry with an empty header" $
      parseFasta "> \nABC" `shouldBe` M.singleton "" "ABC"

    it "Parses an entry with vertical spaces between sequences" $
      parseFasta ">test\nABC\n\nDEF\n\n\n\nGHI\n\n" `shouldBe` M.singleton "test" "ABCDEFGHI"

    it "Parses multiple entries" $
      parseFasta ">test1\nABC\nDEF\n>test2\nGHI\nJKL\n"
      `shouldBe`
      M.fromList [("test1", "ABCDEF"), ("test2", "GHIJKL")]

    it "Parses multiple entries with vertical space" $
      parseFasta ">test1\nABC\nDEF\n\n>test2\nGHI\nJKL\n"
      `shouldBe`
      M.fromList [("test1", "ABCDEF"), ("test2", "GHIJKL")]

    it "Parses 2 entries, one of which is empty and useless" $
      parseFasta "\n>  \n\n>test\nABC" `shouldBe` M.singleton "test" "ABC"
