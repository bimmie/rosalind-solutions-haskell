{-# LANGUAGE ImplicitParams #-}

module Rosalind.Solutions.Tests where

import           Test.HUnit.Approx  ((@~?))
import           Test.Hspec
import           Test.Tasty         (TestTree)
import           Test.Tasty.Hspec   (testSpec)

import           Data.Foldable      (foldl')
import           Rosalind.Fasta     (parseFasta)
import           Rosalind.Solutions

test :: IO [TestTree]
test = sequence
  [ testSpec "nucleotide counts" testDNA
  , testSpec "RNA transcription" testRNA
  , testSpec "Reverse complement" testREVC
  , testSpec "Hamming distance" testHAMM
  , testSpec "GC content" testGC
  , testSpec "Rabbits and Recurrence Relations" testFIB
  , testSpec "Mendel's First Law" testIPRB
  , testSpec "Translating RNA into Protein" testPROT
  , testSpec "Finding a Motif in DNA" testSUBS
  ]


testDNA :: Spec
testDNA =
  describe "Count nucleotides" $ do
    it "Counts 0 from an empty input" $
      nucleotideCounts [] `shouldBe` "0 0 0 0"

    it "Correctly counts the Rosalind example" $
      nucleotideCounts
        (read "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC")
        `shouldBe`
        "20 12 17 21"

testRNA :: Spec
testRNA =
  describe "Transcribe DNA to RNA" $ do
    it "Handles the empty input" $
      transcribe [] `shouldBe` []

    it "Correctly transcribes the Rosalind example" $
      transcribe
        (read "GATGGAACTTGACTACGTAAATT")
        `shouldBe`
        read "GAUGGAACUUGACUACGUAAAUU"

testREVC :: Spec
testREVC =
  describe "Reverse complement a strant of DNA" $ do
    it "Handles empty input" $
      reverseComplement [] `shouldBe` []

    it "Correctly handles the Rosalind example" $
      reverseComplement
        (read "AAAACCCGGT")
        `shouldBe`
        read "ACCGGGTTTT"

testHAMM :: Spec
testHAMM =
  describe "Hamming distance between 2 sequences" $ do
    it "Handles empty input" $
      hamming ([] :: [Int], [] :: [Int]) `shouldBe` 0

    it "Distance 0" $
      hamming ("A", "A") `shouldBe` 0

    it "Distance 1" $
      hamming ("A", "B") `shouldBe` 1

    it "Correctly handles the Rosalind example" $
      hamming ("GAGCCTACTAACGGGAT", "CATCGTAATGACGGCCT") `shouldBe` 7

testGC :: Spec
testGC =
  let
    ?epsilon = 0.00001
  in
  describe "GC content of a DNA sequence" $ do
    it "Handles empty input" $
      gcContent [] `shouldBe` 0.0

    it "Correctly handles the Rosalind example" $
      foldl' max 0 (gcContent . read <$> parseFasta ">Rosalind_6404\n\
                          \CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC\n\
                          \TCCCACTAATAATTCTGAGG\n\
                          \>Rosalind_5959\n\
                          \CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT\n\
                          \ATATCCATTTGTCAGCAGACACGC\n\
                          \>Rosalind_0808\n\
                          \CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC\n\
                          \TGGGAACCTGCGGGCAGTAGGTGGAAT")
      @~?
      60.919540

testFIB :: Spec
testFIB =
  describe "Fibonacci sequence" $ do
    it "Handles the n = 0 base case" $
      rabbits 0 0 `shouldBe` 0

    it "Handles the n = 1 base case" $
      rabbits 1 0 `shouldBe` 1

    it "Handles the n = 2 base case" $
      rabbits 2 0 `shouldBe` 1

    it "Correctly handles the Rosalind example" $
      rabbits 5 3 `shouldBe` 19

    -- 1 ( 1): o
    -- 2 ( 1): O
    -- 3 ( 4): O o o o
    -- 4 ( 7): O O O O o o o
    -- 5 (19): O o o o O O O O o o o O o o o O o o o
    -- 6 (40): O O O O O O O O O O O O O O O O O O O o o o o o o o o o o o o o o o o o o o o o
    it "Handles a worked example" $
      rabbits 6 3 `shouldBe` 40

    -- 1 ( 1): o
    -- 2 ( 1): O
    -- 3 ( 6): O o o o o o
    -- 4 (11): O O O O O O o o o o o
    it "Handles another worked example" $
      rabbits 4 5 `shouldBe` 11

testIPRB :: Spec
testIPRB =
  let
    ?epsilon = 0.00001
  in
    describe "Mendel's first law" $ do
      it "Correctly handles the Rosalind example" $
         0.78333  @~? mendel1 2 2 2

testPROT :: Spec
testPROT =
  describe "Translating RNA into Protein" $ do
    it "Handles an empty input sequence" $
      translate [] `shouldBe` []

    it "Handles the Rosalind example" $
      translate (read "AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA")
      `shouldBe`
      [read "MAMAPRTEINSTRING"]

    it "Handles multiple encoded proteins" $
      translate (read "AUGCCCUAAAUGGUUUAG")
      `shouldBe`
      [read "MP",read "MV"]

testSUBS :: Spec
testSUBS =
  describe "Finding a Motif in DNA" $ do
    it "Finds no motifs in an empty string" $
      sublistlocs "" "" `shouldBe` []

    it "Handles the Rosalind example" $
      sublistlocs "GATATATGCATATACTT" "ATAT" `shouldBe` [2, 4, 10]
