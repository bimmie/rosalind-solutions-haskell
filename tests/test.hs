module Main where

import           Test.Tasty               (defaultMain, testGroup)

import qualified Rosalind.Fasta.Tests
import qualified Rosalind.Solutions.Tests

main :: IO ()
main = do
  solutionsSpecs <- Rosalind.Solutions.Tests.test
  fastaSpecs <- Rosalind.Fasta.Tests.test
  defaultMain $ testGroup "Tests" (solutionsSpecs ++ fastaSpecs)
