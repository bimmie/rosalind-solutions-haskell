module Rosalind.Types ( DNA(..)
                      , RNA(..)
                      , AminoAcid (..)
                      , ProteinTranslation (..)) where

import qualified Text.ParserCombinators.ReadP    as RP
import           Text.ParserCombinators.ReadPrec (lift)
import           Text.Read                       (readListPrec, readPrec)


data DNA
  = DnaA
  | DnaC
  | DnaG
  | DnaT
  deriving (Eq, Ord)

instance Show DNA where
  show DnaA = "A"
  show DnaC = "C"
  show DnaG = "G"
  show DnaT = "T"

readDna :: RP.ReadP DNA
readDna = do
  c <- RP.choice [RP.char 'A', RP.char 'C', RP.char 'G', RP.char 'T']
  case c of
    'A' -> pure DnaA
    'C' -> pure DnaC
    'G' -> pure DnaG
    'T' -> pure DnaT
    _   -> RP.pfail

instance Read DNA where
  readPrec = lift readDna
  readListPrec = lift $ RP.many readDna

data RNA
  = RnaA
  | RnaC
  | RnaG
  | RnaU
  deriving (Eq, Ord)

instance Show RNA where
  show RnaA = "A"
  show RnaC = "C"
  show RnaG = "G"
  show RnaU = "U"

readRna :: RP.ReadP RNA
readRna = do
  c <- RP.choice [RP.char 'A', RP.char 'C', RP.char 'G', RP.char 'U']
  case c of
    'A' -> pure RnaA
    'C' -> pure RnaC
    'G' -> pure RnaG
    'U' -> pure RnaU
    _   -> RP.pfail

instance Read RNA where
  readPrec = lift readRna
  readListPrec = lift $ RP.many readRna

data AminoAcid
  = Alanine        -- A
  | Cysteine       -- C
  | AsparticAcid   -- D
  | GlutamicAcid   -- E
  | Phenylalanine  -- F
  | Glycine        -- G
  | Histidine      -- H
  | Isoleucine     -- I
  | Lysine         -- K
  | Leucine        -- L
  | Methionine     -- M
  | Asparagine     -- N
  | Proline        -- P
  | Glutamine      -- Q
  | Arginine       -- R
  | Serine         -- S
  | Threonine      -- T
  | Valine         -- V
  | Tryptophan     -- W
  | Tyrosine       -- Y
  deriving (Eq, Ord)

instance Show AminoAcid where
  show Alanine       = "A"
  show Cysteine      = "C"
  show AsparticAcid  = "D"
  show GlutamicAcid  = "E"
  show Phenylalanine = "F"
  show Glycine       = "G"
  show Histidine     = "H"
  show Isoleucine    = "I"
  show Lysine        = "K"
  show Leucine       = "L"
  show Methionine    = "M"
  show Asparagine    = "N"
  show Proline       = "P"
  show Glutamine     = "Q"
  show Arginine      = "R"
  show Serine        = "S"
  show Threonine     = "T"
  show Valine        = "V"
  show Tryptophan    = "W"
  show Tyrosine      = "Y"


readAminoAcid :: RP.ReadP AminoAcid
readAminoAcid = do
  c <- RP.choice [ RP.char 'A'
                 , RP.char 'C'
                 , RP.char 'D'
                 , RP.char 'E'
                 , RP.char 'F'
                 , RP.char 'G'
                 , RP.char 'H'
                 , RP.char 'I'
                 , RP.char 'K'
                 , RP.char 'L'
                 , RP.char 'M'
                 , RP.char 'N'
                 , RP.char 'P'
                 , RP.char 'Q'
                 , RP.char 'R'
                 , RP.char 'S'
                 , RP.char 'T'
                 , RP.char 'V'
                 , RP.char 'W'
                 , RP.char 'Y']
  case c of
    'A' -> pure Alanine
    'C' -> pure Cysteine
    'D' -> pure AsparticAcid
    'E' -> pure GlutamicAcid
    'F' -> pure Phenylalanine
    'G' -> pure Glycine
    'H' -> pure Histidine
    'I' -> pure Isoleucine
    'K' -> pure Lysine
    'L' -> pure Leucine
    'M' -> pure Methionine
    'N' -> pure Asparagine
    'P' -> pure Proline
    'Q' -> pure Glutamine
    'R' -> pure Arginine
    'S' -> pure Serine
    'T' -> pure Threonine
    'V' -> pure Valine
    'W' -> pure Tryptophan
    'Y' -> pure Tyrosine
    _   -> RP.pfail

instance Read AminoAcid where
  readPrec = lift readAminoAcid
  readListPrec = lift $ RP.many readAminoAcid

data ProteinTranslation
  = StartCodon
  | Codon AminoAcid
  | StopCodon
  | NotACodon
  deriving (Eq, Ord)
