module Rosalind.Fasta where

import qualified Data.HashMap.Strict as M
import           Data.List.Extra     (trim, wordsBy)
import           Data.Maybe          (catMaybes)

parseFasta :: String -> M.HashMap String String
parseFasta txt = M.fromList $ catMaybes $ parseEntry <$> wordsBy (== '>') txt
  where
    parseEntry :: String -> Maybe (String, String)
    parseEntry e =
      case trim <$> lines e of
        []              -> Nothing
        [""]            -> Nothing
        ("" : [""])     -> Nothing
        (header : seqs) -> Just (header, concat seqs)

