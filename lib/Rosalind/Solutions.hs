{-# LANGUAGE LambdaCase #-}

module Rosalind.Solutions ( gcContent
                          , hamming
                          , mendel1
                          , nucleotideCounts
                          , rabbits
                          , reverseComplement
                          , sublistlocs
                          , transcribe
                          , translate)
where

import           Data.List       (foldl')
import           Data.List.Extra (chunksOf)
import           Data.Map.Strict ((!?))
import qualified Data.Map.Strict as M
import           Data.Maybe      (fromMaybe)
import           Numeric.Extra   (intToFloat)

import           Data.List       (tails)
import           Rosalind.Types  (AminoAcid (..), DNA (..),
                                  ProteinTranslation (..), RNA (..))

count :: Ord a => [a] -> M.Map a Int
count xs = M.fromListWith (+) [(x, 1) | x <- xs]

queryMap :: Ord a => M.Map a Int -> Int -> a -> Int
queryMap m z k =
  fromMaybe z (m !? k)

nucleotideCounts :: [DNA] -> String
nucleotideCounts dnas = unwords $ show . query counts <$> [DnaA, DnaC, DnaG, DnaT]
  where
    counts :: M.Map DNA Int
    counts = count dnas

    query :: M.Map DNA Int -> DNA -> Int
    query m k =
      queryMap m 0 k

transcribe :: [DNA] -> [RNA]
transcribe =
  map (\case
          DnaA -> RnaA
          DnaC -> RnaC
          DnaG -> RnaG
          DnaT -> RnaU
      )

reverseComplement :: [DNA] -> [DNA]
reverseComplement = map complement . reverse
  where
    complement :: DNA -> DNA
    complement DnaA = DnaT
    complement DnaT = DnaA
    complement DnaC = DnaG
    complement DnaG = DnaC

hamming :: Eq a => ([a], [a]) -> Int
hamming (a, b) = foldl' hamming' 0 $ zip a b
  where
    hamming' :: Eq a => Int -> (a, a) -> Int
    hamming' acc (a', b') = if a' == b' then acc else acc + 1

gcContent :: [DNA] -> Float
gcContent dnas = case length dnas of
  0 -> 0.0
  l -> intToFloat (sum $ query <$> [DnaG, DnaC]) / intToFloat l * 100.0
  where
    counts :: M.Map DNA Int
    counts = count dnas

    query :: DNA -> Int
    query = queryMap counts 0

-- In general, the recurrence relation is F_n = F_n-1 + k * F_n-2
rabbits :: Word -> Word -> Word
rabbits n k =
  case n of
    0 -> 0
    1 -> 1
    2 -> 1
    _ -> rabbits (n - 1) k + (k * rabbits (n - 2) k)

mendel1 :: Int -> Int -> Int -> Float
mendel1 k m n = dom_outcomes / tot_outcomes
  where
    pop :: Int
    pop = k + m + n

    tot_outcomes :: Float
    tot_outcomes = intToFloat (pop * (pop - 1) * 4)

    dom_outcomes :: Float
    dom_outcomes = intToFloat ((k * (pop - 1) * 4) + (m * ((k*4) + ((m - 1)*3) + n*2)) + n*(k*4 + m*2))

translate :: [RNA] -> [[AminoAcid]]
translate mrna = reverse $ dropWhile null $ foldl' constructProteins [] $ translate' <$> chunksOf 3 mrna
  where
    constructProteins :: [[AminoAcid]] -> ProteinTranslation -> [[AminoAcid]]
    constructProteins ps NotACodon         = ps
    constructProteins [] StartCodon        = [[Methionine]]
    constructProteins ([] : ps) StartCodon = [Methionine] : ps
    constructProteins (p : ps) StartCodon  = (p ++ [Methionine]) : ps
    constructProteins ps StopCodon         = [] : ps
    constructProteins (p : ps) (Codon a)   = (p ++ [a]) : ps
    constructProteins [] (Codon _)         = []


    translate' :: [RNA] -> ProteinTranslation
    translate' [RnaU, RnaU, RnaU] = Codon Phenylalanine
    translate' [RnaU, RnaU, RnaC] = Codon Phenylalanine
    translate' [RnaU, RnaU, RnaA] = Codon Leucine
    translate' [RnaU, RnaU, RnaG] = Codon Leucine
    translate' [RnaU, RnaC, RnaU] = Codon Serine
    translate' [RnaU, RnaC, RnaC] = Codon Serine
    translate' [RnaU, RnaC, RnaA] = Codon Serine
    translate' [RnaU, RnaC, RnaG] = Codon Serine
    translate' [RnaU, RnaA, RnaU] = Codon Tyrosine
    translate' [RnaU, RnaA, RnaC] = Codon Tyrosine
    translate' [RnaU, RnaA, RnaA] = StopCodon
    translate' [RnaU, RnaA, RnaG] = StopCodon
    translate' [RnaU, RnaG, RnaU] = Codon Cysteine
    translate' [RnaU, RnaG, RnaC] = Codon Cysteine
    translate' [RnaU, RnaG, RnaA] = StopCodon
    translate' [RnaU, RnaG, RnaG] = Codon Tryptophan
    translate' [RnaC, RnaU, RnaU] = Codon Leucine
    translate' [RnaC, RnaU, RnaC] = Codon Leucine
    translate' [RnaC, RnaU, RnaA] = Codon Leucine
    translate' [RnaC, RnaU, RnaG] = Codon Leucine
    translate' [RnaC, RnaC, RnaU] = Codon Proline
    translate' [RnaC, RnaC, RnaC] = Codon Proline
    translate' [RnaC, RnaC, RnaA] = Codon Proline
    translate' [RnaC, RnaC, RnaG] = Codon Proline
    translate' [RnaC, RnaA, RnaU] = Codon Histidine
    translate' [RnaC, RnaA, RnaC] = Codon Histidine
    translate' [RnaC, RnaA, RnaA] = Codon Glutamine
    translate' [RnaC, RnaA, RnaG] = Codon Glutamine
    translate' [RnaC, RnaG, RnaU] = Codon Arginine
    translate' [RnaC, RnaG, RnaC] = Codon Arginine
    translate' [RnaC, RnaG, RnaA] = Codon Arginine
    translate' [RnaC, RnaG, RnaG] = Codon Arginine
    translate' [RnaA, RnaU, RnaU] = Codon Isoleucine
    translate' [RnaA, RnaU, RnaC] = Codon Isoleucine
    translate' [RnaA, RnaU, RnaA] = Codon Isoleucine
    translate' [RnaA, RnaU, RnaG] = StartCodon
    translate' [RnaA, RnaC, RnaU] = Codon Threonine
    translate' [RnaA, RnaC, RnaC] = Codon Threonine
    translate' [RnaA, RnaC, RnaA] = Codon Threonine
    translate' [RnaA, RnaC, RnaG] = Codon Threonine
    translate' [RnaA, RnaA, RnaU] = Codon Asparagine
    translate' [RnaA, RnaA, RnaC] = Codon Asparagine
    translate' [RnaA, RnaA, RnaA] = Codon Lysine
    translate' [RnaA, RnaA, RnaG] = Codon Lysine
    translate' [RnaA, RnaG, RnaU] = Codon Serine
    translate' [RnaA, RnaG, RnaC] = Codon Serine
    translate' [RnaA, RnaG, RnaA] = Codon Arginine
    translate' [RnaA, RnaG, RnaG] = Codon Arginine
    translate' [RnaG, RnaU, RnaU] = Codon Valine
    translate' [RnaG, RnaU, RnaC] = Codon Valine
    translate' [RnaG, RnaU, RnaA] = Codon Valine
    translate' [RnaG, RnaU, RnaG] = Codon Valine
    translate' [RnaG, RnaC, RnaU] = Codon Alanine
    translate' [RnaG, RnaC, RnaC] = Codon Alanine
    translate' [RnaG, RnaC, RnaA] = Codon Alanine
    translate' [RnaG, RnaC, RnaG] = Codon Alanine
    translate' [RnaG, RnaA, RnaU] = Codon AsparticAcid
    translate' [RnaG, RnaA, RnaC] = Codon AsparticAcid
    translate' [RnaG, RnaA, RnaA] = Codon GlutamicAcid
    translate' [RnaG, RnaA, RnaG] = Codon GlutamicAcid
    translate' [RnaG, RnaG, RnaU] = Codon Glycine
    translate' [RnaG, RnaG, RnaC] = Codon Glycine
    translate' [RnaG, RnaG, RnaA] = Codon Glycine
    translate' [RnaG, RnaG, RnaG] = Codon Glycine
    translate' _                  = NotACodon

sublistlocs :: (Eq a) => [a] -> [a] -> [Word]
sublistlocs s t | length t > length s = []
                | null s = []
                | otherwise = foldr (match t) [] $ zip [1..] $ take (length t) <$> tails s
  where
    match :: Eq a => [a] -> (Word, [a]) -> [Word] -> [Word]
    match t' (idx, ls) idxs =
      if ls == t' then
        idx : idxs
      else
        idxs
