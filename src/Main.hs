module Main where

import           Data.List             (nub)
import           System.Console.GetOpt (ArgDescr (NoArg), ArgOrder (Permute),
                                        OptDescr (..), getOpt, usageInfo)
import           System.Environment    (getArgs)
import           System.Exit           (ExitCode (ExitFailure), exitSuccess,
                                        exitWith)
import           System.IO             (hPutStrLn, stderr)

import           Data.Either           (fromRight)
import qualified Data.HashMap.Strict   as M
import           Rosalind.Fasta        (parseFasta)
import qualified Rosalind.Solutions    as R
import           Rosalind.Types        (DNA)
import           Text.Read             (readEither, readMaybe)

data Flag
  = Dna
  | Rna
  | Revc
  | Fib
  | Hamm
  | Gc
  | Iprb
  | Prot
  | Subs
  | Help
  deriving (Eq,Ord,Enum,Show,Bounded)

flags :: [OptDescr Flag]
flags =
  [ Option ['1'] ["DNA"] (NoArg Dna)
      "Solve \"Counting DNA Nucleotides\"."
  , Option ['2'] ["RNA"] (NoArg Rna)
      "Solve \"Transcribing DNA into RNA\"."
  , Option ['3'] ["REVC"] (NoArg Revc)
      "Solve \"Complementing a Strand of DNA\"."
  , Option ['4'] ["FIB"] (NoArg Fib)
      "Solve \"Rabbits and Recurrence Relations\"."
  , Option ['5'] ["GC"] (NoArg Gc)
      "Solve \"Computing GC Content\"."
  , Option ['6'] ["HAMM"] (NoArg Hamm)
      "Solve \"Counting Point Mutations\"."
  , Option ['7'] ["IPRB"] (NoArg Iprb)
      "Solve \"Mendel's First Law\"."
  , Option ['8'] ["PROT"] (NoArg Prot)
      "Solve \"Translating RNA into Protein\"."
  , Option ['9'] ["SUBS"] (NoArg Subs)
      "Solve \"Finding a Motif in DNA\"."
  , Option ['h', '?'] ["help"] (NoArg Help)
      "Print this help message and exit."
  ]

parseArgs :: [String] -> IO ([Flag], [String])
parseArgs argv = case getOpt Permute flags argv of

  (args, fs, []) -> do
    let files = if null fs then ["-"] else fs
    if Help `elem` args || null args
      then do
        hPutStrLn stderr (usageInfo header flags)
        exitSuccess
      else
        pure (nub args, files)

  (_, _, errs) -> do
    hPutStrLn stderr (concat errs ++ usageInfo header flags)
    exitWith (ExitFailure 1)
  where
    header = "Usage: solutions [-12345678h?] [file...]"

displayOutput :: Either String String -> IO ()
displayOutput = putStrLn . fromRight "ERR: Not valid input"

solveDna :: String -> IO ()
solveDna input = displayOutput $ R.nucleotideCounts <$> readEither input

solveRna :: String -> IO ()
solveRna input = displayOutput $ concatMap show . R.transcribe <$> readEither input

solveRevc :: String -> IO ()
solveRevc input = displayOutput $ concatMap show . R.reverseComplement <$> readEither input

solveFib :: String -> IO ()
solveFib input = case readEither <$> words input of
  (Right x : Right y : _) -> print $ R.rabbits x y
  _                       -> putStrLn "Could not parse input"

solveHamm :: String -> IO ()
solveHamm input = displayOutput $
                  show . R.hamming <$>
                  ((sequence (readEither <$> lines input) :: Either String [[DNA]]) >>= take2)
  where
    take2 :: [a] -> Either String (a, a)
    take2 xs = case xs of
      []      -> Left "Not enough data"
      [_]     -> Left "Not enough data"
      (x:y:_) -> Right (x, y)

solveGc :: String -> IO ()
solveGc input = putStrLn $ fst highest ++ "\n" ++ show (snd highest)
  where
    accumulateHighest :: (String, Float) -> String -> Float -> (String, Float)
    accumulateHighest acc@(_, v') k v =
      if v > v' then
        (k, v)
      else
        acc

    highest :: (String, Float)
    highest = M.foldlWithKey' accumulateHighest ("", 0) $
              R.gcContent <$>
              M.mapMaybe readMaybe (parseFasta input)

solveIprb :: String -> IO ()
solveIprb input = case sequence $ readEither <$> words input of
  Right (k : m : n : _) -> print (R.mendel1 k m n)
  _                     -> putStrLn "Invalid input format"

solveProt :: String -> IO ()
solveProt input = case R.translate <$> readEither input of
  Right prots -> putStrLn $ unlines $ concatMap show <$> prots
  Left msg    -> putStrLn msg

solveSubs :: String -> IO ()
solveSubs input = case lines input of
  (s : t : _) -> putStrLn $ unwords $ show <$> R.sublistlocs s t
  _           -> putStrLn "ERR: Expected 2 lines of text input"

solveProblem :: (Flag, String) -> IO ()
solveProblem (f, filename) = do
  input <- if filename == "-" then
             getLine
           else
             readFile filename
  case f of
    Dna  -> solveDna input
    Rna  -> solveRna input
    Revc -> solveRevc input
    Fib  -> solveFib input
    Hamm -> solveHamm input
    Gc   -> solveGc input
    Iprb -> solveIprb input
    Prot -> solveProt input
    Subs -> solveSubs input
    _    -> putStrLn "ERR: Not a Rosalind Problem"



solve :: ([Flag], [String]) -> IO ()
solve (as, fs) = mapM_ solveProblem [(a, f) | a <- as, f <- fs]

main :: IO ()
main = getArgs >>= parseArgs >>= solve
